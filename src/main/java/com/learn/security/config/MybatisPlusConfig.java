package com.learn.security.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @auther liuhw
 * @date 2020/3/17 09:33
 * @description:
 * @since 1.0
 **/
@Configuration
public class MybatisPlusConfig {
    /**
     * sql格式化拦截器
     * @return
     */
    @Bean
    public PerformanceInterceptor performanceInterceptor() {
        PerformanceInterceptor bean = new PerformanceInterceptor();
        bean.setFormat(true);
        return bean;
    }

    /**
     * 分页拦截器
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
