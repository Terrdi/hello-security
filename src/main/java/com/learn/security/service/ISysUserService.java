package com.learn.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.learn.security.domain.SysUser;

/**
 * @auther liuhw
 * @date 2020/3/17 10:55
 * @description:
 * @since 1.0
 **/
public interface ISysUserService extends IService<SysUser> {
    SysUser getByName(String username);
}
