package com.learn.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.learn.security.dao.SysRoleMapper;
import com.learn.security.domain.SysRole;
import com.learn.security.service.ISysRoleService;
import org.springframework.stereotype.Service;

/**
 * @auther liuhw
 * @date 2020/3/17 11:35
 * @description:
 * @since 1.0
 **/
@Service
public class SysRoleService extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
    @Override
    public SysRole getByName(String name) {
        return baseMapper.getByName(name);
    }
}
