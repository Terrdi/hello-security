package com.learn.security.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.learn.security.dao.SysUserMapper;
import com.learn.security.domain.SysUser;
import com.learn.security.service.ISysUserService;
import org.springframework.stereotype.Service;

/**
 * @auther liuhw
 * @date 2020/3/17 10:58
 * @description:
 * @since 1.0
 **/
@Service
public class SysUserService extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {
    @Override
    public SysUser getByName(String username) {
        return this.getOne(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getName, username));
    }
}
