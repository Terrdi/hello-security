package com.learn.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.learn.security.dao.SysPermissionMapper;
import com.learn.security.domain.SysPermission;
import com.learn.security.service.ISysPermissionService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @auther liuhw
 * @date 2020/3/22 21:26
 * @description:
 * @since 1.0
 **/
@Service
public class SysPermissionService extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {
    @Override
    public List<SysPermission> listByRoleId(Integer roleID) {
        return baseMapper.listByRoleId(roleID);
    }
}
