package com.learn.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.learn.security.dao.SysUserRoleMapper;
import com.learn.security.domain.SysUserRole;
import com.learn.security.service.ISysUserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @auther liuhw
 * @date 2020/3/17 11:12
 * @description:
 * @since 1.0
 **/
@Service
public class SysUserRoleService extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {
    public List<SysUserRole> listByUserId(Integer id) {
        return this.list(Wrappers.<SysUserRole>lambdaQuery().eq(SysUserRole::getUserId, id));
    }
}
