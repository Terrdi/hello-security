package com.learn.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.learn.security.domain.SysRole;

/**
 * @version 1.0
 * @auther <a href="mailto:liuhy@tydic.com">liuhw</a>
 * @date 2020/3/17 11:34
 * @description:
 **/
public interface ISysRoleService extends IService<SysRole> {
    SysRole getByName(String name);
}
