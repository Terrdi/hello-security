package com.learn.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.learn.security.domain.SysPermission;

import java.util.List;

/**
 * @auther liuhw
 * @date 2020/3/22 21:25
 * @description:
 * @since 1.0
 **/
public interface ISysPermissionService extends IService<SysPermission> {
    List<SysPermission> listByRoleId(Integer roleID);
}
