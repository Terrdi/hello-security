package com.learn.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.learn.security.domain.SysUserRole;

/**
 * @version 1.0
 * @auther <a href="mailto:liuhy@tydic.com">liuhw</a>
 * @date 2020/3/17 11:11
 * @description:
 **/
public interface ISysUserRoleService extends IService<SysUserRole> {
}
