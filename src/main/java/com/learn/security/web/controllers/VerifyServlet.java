package com.learn.security.web.controllers;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.learn.security.common.VerifyConstant;
import org.springframework.util.Assert;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

/**
 * @auther liuhw
 * @date 2020/3/21 19:29
 * @description:
 * @since 1.0
 **/
public class VerifyServlet extends HttpServlet {
    /**
     * 图片的宽度
     */
    private int width = 100;

    /**
     * 图片的高度
     */
    private int height = 30;

    /**
     * 验证码字符个数
     */
    private int codeCount = 4;

    /**
     * 字符高度
     */
    private int fontHeight;

    /**
     * 干扰线个数
     */
    private int interLine = 4;

    /**
     * 第一个字符的x轴值, 因为后面的字符依次递增, 所以它们的x轴值是codeX的倍数
     */
    private int codeX;

    /**
     * 验证字符的y轴值, 因为并行, 所以值一样
     */
    private int codeY;

    /**
     * 字符允许出现的序列值
     */
    private char[] codeSequence = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z',
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
    };

    @Override
    public void init() throws ServletException {
        super.init();
        // 从web.xml中获取初始信息
        // 宽度
        String strWidth = this.getInitParameter("width");
        // 高度
        String strHeight = this.getInitParameter("height");
        // 字符个数
        String strCodeCount = this.getInitParameter("codeCount");

        try {
            if (StringUtils.isNotEmpty(strWidth)) {
                width = Integer.parseInt(strWidth);
            }
            if (StringUtils.isNotEmpty(strHeight)) {
                height = Integer.parseInt(strHeight);
            }
            if (StringUtils.isNotEmpty(strCodeCount)) {
                codeCount = Integer.parseInt(strCodeCount);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        // width - 4 出去左右多余的位置, 使验证码更加集中的显示, 减得越多越集中
        // codeCount + 1 等比分配显示的宽度, 包括左右两边的空格
        codeX = (width - 4) / (codeCount + 1);
        // height - 10 集中显示验证码
        fontHeight = height - 10;
        codeY = height - 7;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 定义图像buffer
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D gd = image.createGraphics();

        // 创建一个随机数生成类
        Random random = new Random();
        // 将图像填充为白色
        gd.setColor(Color.LIGHT_GRAY);
        gd.fillRect(0, 0, width, height);
        // 创建字体, 字体的大小根据图片的高度来定
        Font font = new Font("Times New Roman", Font.PLAIN, fontHeight);
        // 设置字体
        gd.setFont(font);
        gd.setColor(Color.BLACK);
        gd.drawRect(0, 0, width - 1, height - 1);
        // 随机产生干扰线, 使图像中的认证码不被其它程序检测到。
        gd.setColor(Color.GRAY);
        for (int i = 0; i < interLine; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int x1 = random.nextInt(12);
            int y1 = random.nextInt(12);
            gd.drawLine(x, y, x1, y1);
        }

        // randomCode 用于保存随机产生的验证码, 以便用户登录验证
        StringBuilder randomCode = new StringBuilder();
        int red = 0, green = 0, blue = 0;
        // 随机产生codeCount个数字的验证码
        for (int i = 0; i < codeCount; i++) {
            // 得到随机产生的验证码数字
            String strRand = String.valueOf(codeSequence[random.nextInt(codeSequence.length)]);
            // 产生随机的颜色分量来构造颜色值
            red = random.nextInt(255);
            green = random.nextInt(255);
            blue = random.nextInt(255);
            // 用随机产生的颜色将验证码绘制到图像中
            gd.setColor(new Color(red, green, blue));
            gd.drawString(strRand, (i + 1) * codeX, codeY);
            // 将codeCount个随机数组合在一起
            randomCode.append(strRand);
        }

        // 将四位数验证码保存到Session中
        HttpSession session = req.getSession();
        session.setAttribute(VerifyConstant.VERIFY_CODE_NAME, randomCode.toString());
        System.out.println("生成验证码 " + randomCode);

        // 禁止图像缓存
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Cache-Control", "no-cache");
        resp.setDateHeader("Expires", 0);

        resp.setContentType("image/jpeg");
        // 将图像输出到Servlet输出流中
        ServletOutputStream stream = resp.getOutputStream();
        ImageIO.write(image, "jpeg", stream);
        stream.close();
    }
}
