package com.learn.security.web.controllers;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @auther liuhw
 * @date 2020/3/20 16:54
 * @description: 处理异常的控制器
 * @since 1.0
 **/
@Controller
public class ErrorController {
    @RequestMapping("/login/error")
    public void loginError(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=utf-8");
        AuthenticationException exception = (AuthenticationException) request.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);

        try (PrintWriter pw = response.getWriter()) {
            pw.println(exception.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
