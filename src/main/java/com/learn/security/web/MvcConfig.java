package com.learn.security.web;

import com.learn.security.web.controllers.VerifyServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @auther liuhw
 * @date 2020/3/16 14:43
 * @description:
 * @since 1.0
 **/
@Configuration
public class MvcConfig extends WebMvcConfigurationSupport {
//    @Override
//    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
//        super.addResourceHandlers(registry);
//    }

    @Override
    protected void addViewControllers(ViewControllerRegistry registry) {
        super.addViewControllers(registry);
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/hello").setViewName("hello");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/getVerifyCode").setViewName("getVerifyCode");
    }

    /**
     * 注入 com.learn.security.web.controllers.VerifyServlet
     * @return
     */
    @Bean
    public ServletRegistrationBean<VerifyServlet> indexServletRegistration() {
        //        registrationBean.addUrlMappings("/getVerifyCode"); // /getVerifyCode
        return new ServletRegistrationBean<>(new VerifyServlet(), "/getVerifyCode");
    }

    @Bean
    public RequestContextListener requestContextListener(){
        return new RequestContextListener();
    }

}
