package com.learn.security.web.filters;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

/**
 * @auther liuhw
 * @date 2020/3/22 16:22
 * @description:
 * @since 1.0
 **/
public class VerifyWebAuthenticationDetails extends WebAuthenticationDetails {
    private final String verifyCode;
    /**
     * Records the remote address and will also set the session Id if a session already
     * exists (it won't create one).
     *
     * @param request that the authentication request was received from
     */
    public VerifyWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        verifyCode = request.getParameter("verifyCode");
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append("; VerifyCode: ").append(this.getVerifyCode());
        return sb.toString();
    }
}
