package com.learn.security.web.filters;

import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @auther liuhw
 * @date 2020/3/22 16:45
 * @description:
 * @since 1.0
 **/
@Component
public class VerifyAuthenticationDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails> {
    @Override
    public WebAuthenticationDetails buildDetails(HttpServletRequest context) {
        return new VerifyWebAuthenticationDetails(context);
    }
}
