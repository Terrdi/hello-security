package com.learn.security.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @auther liuhw
 * @date 2020/3/17 09:53
 * @description:
 * @since 1.0
 **/
@Getter
@Setter
@ToString
public class SysUser implements Serializable {
    @TableId
    private Integer id;

    private String name;

    private String password;
}
