package com.learn.security.domain;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * @auther liuhw
 * @date 2020/3/22 21:13
 * @description:
 * @since 1.0
 **/
@Getter
@Setter
public class SysPermission implements Serializable {
    private Integer id;

    private String url;

    private Integer roleId;

    private String permission;

    public List<String> getPermissions() {
        return Arrays.asList(StringUtils.split(this.getPermission().trim(), ","));
    }

    @Override
    public String toString() {
        return url + " -> [" + permission + ']';
    }
}
