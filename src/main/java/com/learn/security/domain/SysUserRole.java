package com.learn.security.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @auther liuhw
 * @date 2020/3/17 09:58
 * @description: 关联用户与角色
 * @since 1.0
 **/
@Getter
@Setter
@ToString
public class SysUserRole implements Serializable {
    private Integer userId;

    private Integer roleId;
}
