package com.learn.security.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @auther liuhw
 * @date 2020/3/17 09:57
 * @description: 角色
 * @since 1.0
 **/
@Getter
@Setter
@ToString
public class SysRole implements Serializable {
    @TableId
    private Integer id;

    private String name;
}
