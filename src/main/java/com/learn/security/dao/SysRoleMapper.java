package com.learn.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.learn.security.domain.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @version 1.0
 * @auther <a href="mailto:liuhy@tydic.com">liuhw</a>
 * @date 2020/3/17 11:32
 * @description:
 **/
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
    @Select("SELECT * FROM sys_role WHERE name = #{name}")
    SysRole getByName(String name);
}
