package com.learn.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.learn.security.domain.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @auther liuhw
 * @date 2020/3/17 10:01
 * @description:
 * @since 1.0
 **/
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
