package com.learn.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.learn.security.domain.SysPermission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @version 1.0
 * @auther <a href="mailto:liuhy@tydic.com">liuhw</a>
 * @date 2020/3/22 21:20
 * @description:
 **/
@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission> {
    default List<SysPermission> listByRoleId(Integer id) {
        return this.selectList(Wrappers.<SysPermission>lambdaQuery().eq(SysPermission::getRoleId, id));
    }
}
