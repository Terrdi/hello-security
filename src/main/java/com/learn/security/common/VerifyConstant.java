package com.learn.security.common;

/**
 * @version 1.0
 * @auther <a href="mailto:liuhy@tydic.com">liuhw</a>
 * @date 2020/3/21 21:58
 * @description:
 **/
public interface VerifyConstant {
    String VERIFY_CODE_NAME = "validateCode";
}
