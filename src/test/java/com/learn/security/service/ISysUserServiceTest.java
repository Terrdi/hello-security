package com.learn.security.service;

import com.learn.security.domain.SysUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ISysUserServiceTest {
    @Autowired
    private ISysUserService sysUserService;

    @Test
    public void testSelectById() {
        SysUser user = sysUserService.getById(1);
        Assert.assertNotNull(user);
        user = sysUserService.getById(5);
        Assert.assertNull(user);
    }
}