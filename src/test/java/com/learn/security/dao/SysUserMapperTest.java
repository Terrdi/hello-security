package com.learn.security.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.learn.security.domain.SysUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SysUserMapperTest {
    @Autowired
    private SysUserMapper userMapper;

    @Test
    public void testListAll() {
        SysUser user = new SysUser();
//        user.setId(1);
        Wrapper<SysUser> userWrapper = new QueryWrapper<>();
        Assert.assertEquals(3, userMapper.selectList(userWrapper).size());
    }
}